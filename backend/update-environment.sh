#!/bin/bash

pip-compile --extra-index-url https://google-coral.github.io/py-repo/ --generate-hashes requirements.in

if [[ $? -ne 0 ]]; then
    echo "pip-compile failed. Build failed"
    exit 1
fi

pip install -r requirements.txt
