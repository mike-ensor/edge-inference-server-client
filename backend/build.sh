#!/bin/bash

VERSION=$1

if [[ -z "${VERSION}" ]]; then
    echo "NO VERISON NUMBER SPECIFIED"
    exit 0
fi

docker build -t edge-demo .
docker tag edge-demo us-west1-docker.pkg.dev/${PROJECT_ID}/demos/edge-demo:v${VERSION}
docker tag edge-demo us-west1-docker.pkg.dev/${PROJECT_ID}/demos/edge-demo:latest
docker push us-west1-docker.pkg.dev/${PROJECT_ID}/demos/edge-demo:v${VERSION}
docker push us-west1-docker.pkg.dev/${PROJECT_ID}/demos/edge-demo:latest
