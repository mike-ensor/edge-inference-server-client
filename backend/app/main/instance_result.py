

class FrameInstanceResult:

    def __init__(self, orig_frame, isFound  = False):
        self.found = isFound
        self.orig_frame = orig_frame
        self.topBoundTuple = ()
        self.bottomBoundTuple = ()

    def isFound(self) -> bool:
        return self.found

    def setFound(self):
        self.found = True

    def setTopBound(self, topBoundTuple: tuple):
        self.topBoundTuple = topBoundTuple

    def setBottomBound(self, bottomBoundTuple: tuple):
        self.bottomBoundTuple = bottomBoundTuple

    def getTopBoundTuple(self) -> tuple:
        if self.found:
            return self.topBoundTuple
        else:
            return ()

    def getOriginalFrame(self):
        return self.orig_frame

    def getBottomBoundTuple(self) -> tuple:
        if self.found:
            return self.bottomBoundTuple
        else:
            return ()

    def __str__(self) -> str:
        return "Found: " + str(self.found) + " Top: " + str(self.topBoundTuple) + " Bottom: " + str(self.bottomBoundTuple)