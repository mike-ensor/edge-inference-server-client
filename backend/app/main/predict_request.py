import base64
import json
from typing import List

class b64Image:
    b64: str

    def __init__(self, image_bytes):
        # check if encoded_image is binary
        if not isinstance(image_bytes, bytes):
            raise TypeError("image_bytes MUST be a byte string")

        self.b64 = base64.b64encode(image_bytes).decode()

    def json(self):
        return json.dumps(self, default=lambda q: q.__dict__)

class ImageInstance:
    key: str
    image_bytes: str

    def __init__(self, key, b64Image):
        self.key = key
        self.image_bytes = b64Image

    def json(self):
        return json.dumps(self, default=lambda x: x.__dict__)

class PredictRequest:

    instances: List[ImageInstance]

    def __init__(self, instances = []):
        self.instances = instances

    def addImage(self, imageInstance):
        self.instances.append(imageInstance)

    def json(self):
        return json.dumps(self, default=lambda o: o.__dict__)
