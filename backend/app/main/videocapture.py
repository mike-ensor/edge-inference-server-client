import errno
import logging
from threading import Thread
from typing import Type
import cv2

from app.main.remote_inference import RemoteFrameInference
from app.main.inference_plan import InferencePlan
from app.main.instance_result import FrameInstanceResult

log = logging.getLogger(__name__)


class VideoCapture:

    """
    This class is used to capture video from a source (webcam or video file)
    """

    stopped = False

    def __init__(self, plan: Type[InferencePlan], pipe):
        self.stream = plan.getStreamURL()
        self.plan = plan
        self.pipe = pipe
        self.stopped = True

        # opening video capture stream
        self.vcap = cv2.VideoCapture(self.stream)
        self.vcap.set(cv2.CAP_PROP_BUFFERSIZE, 1)  # Set buffer for 1 frame

        if self.vcap.isOpened() is False:
            log.error("[Exiting]: Error accessing webcam stream.")
            exit(0)

        self.grabbed, self.frame = self.vcap.read()

        if self.grabbed is False:
            log.error("[Exiting] No more frames to read")
            exit(0)

        # thread instantiation # TODO: Would Process vs Thread be better for performance?
        self.t = Thread(target=self.update, args=())
        self.t.daemon = True  # daemon threads run in background

    # method to start thread
    def start(self):
        self.stopped = False
        self.t.start()

    # method passed to thread to read next available frame
    def update(self) -> bytearray:
        counter = 0
        while True:
            if self.stopped is True:
                log.warn("Stopping Video Capture")
                break
            grabbed, frame = self.vcap.read()

            if grabbed is False:
                log.error("Video frame not found")
                continue
            else:
                # TODO: Get the right type of inferencing to use (remote vs local vs edgeTPU)
                inference = RemoteFrameInference("hat")
                skip = self.plan.skip()
                if not skip:
                    framesToSkip = self.plan.framesToSkip()
                    mod = counter % framesToSkip == 0
                    if mod:
                        result = inference.predict(frame)
                else:
                    # skip inferencing
                    result = FrameInstanceResult(frame)
                try:
                    self.pipe.send(result)
                except IOError as e:
                    if e.errno == errno.EPIPE:
                        pass
            counter += 1

        self.vcap.release()

    # method to return latest read frame
    def read(self):
        return self.frame

    # method to stop reading frames
    def stop(self):
        self.t.stop()
        self.stopped = True
