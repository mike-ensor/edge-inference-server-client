import logging
from typing import Type
import cv2
import requests
from app.main.inference_frame import InferenceFrame
from app.main.inferenceserver import HTTPInferenceServer
from app.main.instance_result import FrameInstanceResult
from app.main.predict_request import ImageInstance, PredictRequest, b64Image
from config import Config

log = logging.getLogger(__name__)

RESIZE_PCT = Config.INFERENCE_RESIZE_PCT
CLASSIFIER_THRESHOLD = Config.INFERENCE_RESULT_THRESHOLD
HEADERS_JSON = {"content-type": "application/json"}


class RemoteFrameInference(InferenceFrame):
    def __init__(self, classifier_label=""):
        self.inference_server = HTTPInferenceServer(
            Config.INFERENCE_SERVER_HOST,
            Config.INFERENCE_SERVER_PORT,
            Config.INFERENCE_SERVER_MODEL_NAME,
            Config.INFERENCE_SERVER_MODEL_LABEL,
        )
        self.classifier_label = classifier_label

    def predict(self, frame) -> Type[FrameInstanceResult]:
        # Create a result object to contain results (defaults to not found)
        inferenceResult = FrameInstanceResult(frame)

        height, width, _ = frame.shape

        # Resize the image
        resized_jpg = self._resizeJPGForInference(frame)

        # Send to Inference
        predictions = self._inference_frame(resized_jpg.tobytes())

        # Process inferences
        if predictions is not None:
            # At least something was found, may not be at the threshold
            for prediction in predictions:
                num_detections = int(prediction["num_detections"])
                box_data = prediction["detection_boxes"]
                scores = prediction["detection_scores"]

                for i in range(num_detections):
                    score = int(scores[i] * 100)
                    if score > CLASSIFIER_THRESHOLD:
                        dimensions = box_data[i]

                        top = int(dimensions[0] * height)  # TOP
                        left = int(dimensions[1] * width)  # LEFT
                        bottom = int(dimensions[2] * height)  # RIGHT
                        right = int(dimensions[3] * width)  # BOTTOM

                        inferenceResult.setFound()

                        topLeft = (left, top)
                        bottomRight = (right, bottom)
                        inferenceResult.setTopBound(topLeft)
                        inferenceResult.setBottomBound(bottomRight)

                        log.debug(
                            f"Score: {score} -- (Top Left: {left}, {top}, Bottom Right: {right}, {bottom})"
                        )

        return inferenceResult

    def _inference_frame(self, byte_image):
        # Create b64Image
        img = b64Image(byte_image)
        # Create ImageInstance
        imageInstance = ImageInstance(self.classifier_label, img)
        # Ceate Request
        predictRequest = PredictRequest([imageInstance])

        try:
            response = requests.post(
                self.inference_server.getPredict(),
                data=predictRequest.json(),
                headers=HEADERS_JSON,
            )
        except requests.exceptions.RequestException as e:
            log.error(f"Inference Server Not Found: {e}")
            return None

        if response.status_code == 200:
            # Remove the batched responses, only take the first one
            return response.json()["predictions"]
        else:
            return None

    def _resizeJPGForInference(self, frame):
        # preprocess the image and prepare it for classification
        height, width, _ = frame.shape
        fWidth = int(width * RESIZE_PCT / 100)
        fHeight = int(height * RESIZE_PCT / 100)
        dim = (fWidth, fHeight)

        resized_image = cv2.resize(
            frame, dim, interpolation=cv2.INTER_LINEAR
        )  # Percentage of the size
        (_, resized_jpg) = cv2.imencode(".jpg", resized_image)

        return resized_jpg
