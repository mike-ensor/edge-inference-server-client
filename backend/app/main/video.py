import base64
import logging
import multiprocessing
import queue
import threading
from typing import Type
import cv2
import requests
from app.main.capture_strategies import (
    EveryFrameInferenceStrategy,
    NoFrameInferenceStrategy,
    SlowFrameInferenceStrategy,
)
from app.main.inference_plan import InferencePlan
from app.main.instance_result import FrameInstanceResult

from app.main.videocapture import VideoCapture
from config import Config

log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)

threadLock = threading.Lock()
# Hold state of is-capturing
CAPTURE = False
LIGHT_STATUS = False


def getInferencePlan(name, feed_url) -> Type[InferencePlan]:
    if Config.INFERENCE_STRATEGY_FULLFRAME == name:
        log.info("Using FullFrame inference plan")
        return InferencePlan(feed_url, EveryFrameInferenceStrategy())
    elif Config.INFERENCE_STRATEGY_SLOWFRAME == name:
        log.info("Using Slow Frame inference plan")
        return InferencePlan(feed_url, SlowFrameInferenceStrategy())
    else:
        log.info("Using NO frame inference plan")
        return InferencePlan(feed_url, NoFrameInferenceStrategy())


def stop_video():
    """
    Stops the Video Feed thread
    """
    global CAPTURE
    
    log.warn("Stopping Feed")
    with threadLock:
        CAPTURE = False
        log.warn("Stopped Feed")


def video_feed(feed_url):
    """
    Starts the Video Feed thread
    """
    global CAPTURE

    pipe = multiprocessing.Pipe()
    plan = getInferencePlan(Config.INFERENCE_STRATEGY, feed_url)

    with threadLock:
        capture = VideoCapture(plan, pipe[0])
        capture.start()
        CAPTURE = True

    while CAPTURE is True:
        result: Type[FrameInstanceResult] = pipe[1].recv()
        frame = result.getOriginalFrame()

        if result.isFound():
            start_point = result.getTopBoundTuple()
            end_point = result.getBottomBoundTuple()

            color = (255, 0, 0)  # blue
            thickness = 2
            frame = cv2.rectangle(frame, start_point, end_point, color, thickness)
            # TODO: Setup a method to only call once and a while

            if Config.LIGHT_SERVICE_STATE == "true":
                set_light_state("green", "on")
        else:
            # TODO: Setup a buffer for this so it does not just flicker
            if Config.LIGHT_SERVICE_STATE == "true":
                set_light_state("green", "off")

        # encode the frame in JPEG format
        (_, frame_jpg) = cv2.imencode(".jpg", frame)

        out_frame = frame_jpg.tobytes()

        yield (b"--frame\r\n" b"Content-type: image/jpeg\r\n\r\n" + out_frame + b"\r\n")

    log.warn("Feed reset")

def decode_stream_url(url):
    # base64 decode URL
    decoded_url = base64.b64decode(url.strip())
    return decoded_url.decode("ascii")


def set_light_state(color, state):
    """Sets the state of <color> light to <state>, assuming that the Web Relay Controller is listening at "ip_address"
    Args:
        color (str): The color of the relay to control. Can be "red", "blue", or "green".
        state (str): The state of the relay to set. Can be "on" or "off".
    Returns:
        A requests.Response object.
    """
    global LIGHT_STATUS

    ip_address = Config.LIGHT_SERVICE_IP
    color_to_relay = {"red": "relay1", "blue": "relay3", "green": "relay2"}
    state_to_value = {"on": 1, "off": 0}
    light_state = float(state_to_value[state])

    request_url = "http://{}/state.json?{}={}".format(
        ip_address, color_to_relay[color], light_state
    )

    run = False
    # with threadLock:
    if LIGHT_STATUS is False and light_state == 1.0:
        LIGHT_STATUS = True
        log.info("Turning ON light")
        run = True
    elif LIGHT_STATUS is True and light_state == 0.0:
        log.info("Turning OFF light")
        LIGHT_STATUS = False
        run = True
    try:
        if run is True:
            requests.get(request_url, timeout=0.1)
    except requests.exceptions.ReadTimeout as e:
        log.info(e)
        pass

    return
