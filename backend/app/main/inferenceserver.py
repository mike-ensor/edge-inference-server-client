import logging

log = logging.getLogger(__name__)

class HTTPInferenceServer:
    def __init__(self, host, port, model_name, model_label):
        self.host = host
        self.port = port
        self.model_name = model_name
        self.model_label = model_label

    def getPredict(self):
        # "http://localhost:8501/v1/models/<model-name>/[versions/<version>][labels/<label-name>]:predict"
        if self.model_label is not None and self.model_label != "":
            log.debug("Using model label: %s", self.model_label)
            return f"http://{self.host}:{self.port}/v1/models/{self.model_name}/labels/{self.model_label}:predict"
        else:
            return f"http://{self.host}:{self.port}/v1/models/{self.model_name}:predict"

    def getInference(self):
        print("UNDEFINED")
        return f"UNDEFINED"
