import logging

import requests
from app.main import bp
import json
from flask import (
    Response,
    jsonify,
    redirect,
    render_template,
    request,
    send_file,
)
from flask_cors import cross_origin

from config import Config

log = logging.getLogger(__name__)

from app.main.video import decode_stream_url, stop_video, video_feed


# Produces the <img/> stream
@bp.route("/stream", methods=["GET"])
@cross_origin()
def stream():
    """Returns a stream of JPG images from a source. Takes a base64 encoded URL as a query parameter."""
    url_param = request.args.get("url")
    if url_param is None or url_param == "":
        return send_file("static/no-feed.jpg", mimetype="image/jpeg")
    else:
        url = decode_stream_url(url_param)
        return Response(
            video_feed(url), mimetype="multipart/x-mixed-replace; boundary=frame"
        )


# Creates a URL to stream back as JPG
@bp.route("/start", methods=["POST", "OPTIONS"])
@cross_origin()
def start_feed():
    content_type = request.headers.get("Content-Type")
    if request.method == "OPTIONS":
        response.headers.add("Access-Control-Allow-Origin", "*")
        return response

    if content_type == "application/json":
        data = json.loads(request.data)
        url = data["url"]
        response = jsonify({"rtsp": url})
        return response
    else:
        return "Content-Type not supported!"


# Stops the video
@bp.route("/stop", methods=["GET"])
@cross_origin()
def stop_feed():
    log.info("Stopping Feed")

    stop_video()

    return redirect("/", code=302)


# Home page
@bp.route("/", methods=["GET"])
def index():
    log.info(
        "Checking internet connectivity setting: {}".format(Config.SHOW_INTERNET_STATUS)
    )
    return render_template(
        "index.html",
        show_internet=Config.SHOW_INTERNET_STATUS,
        page_title=Config.PAGE_TITLE,
        display_model_name=Config.DISPLAYED_MODEL_NAME,
        default_stream_url=Config.DEFAULT_STREAM_URL,
        splash_image=Config.SPLASH_IMAGE,
    )


@bp.route("/favicon.ico")
def favicon():
    return send_file("static/favicon.ico", mimetype="image/vnd.microsoft.icon")


@bp.route("/internet-status", methods=["GET"])
@cross_origin()
def internet_status():
    try:
        result = requests.get("https://google.com", timeout=1)
        return str(result.status_code == 200)
    except requests.exceptions.RequestException as e:
        return "500"
