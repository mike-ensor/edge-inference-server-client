from typing import Type

from app.main.capture_strategies import InferenceStrategy

class InferencePlan:
    def __init__(self, feed_url, inference_strategy: Type[InferenceStrategy]):
        self.feed_url = feed_url
        self.strategy = inference_strategy

    def skip(self) -> bool:
        return self.strategy.execute() == 0 # skip if zero, otherwise run

    def framesToSkip(self) -> int:
        return self.strategy.execute()

    def getStreamURL(self) -> str:
        return self.feed_url
