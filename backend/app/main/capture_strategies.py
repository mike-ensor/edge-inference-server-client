from abc import abstractmethod


class InferenceStrategy:
    def __init__(self) -> None:
        pass

    @abstractmethod
    def execute(self) -> int:
        pass


class EveryFrameInferenceStrategy(InferenceStrategy):

    def __init__(self) -> None:
        super().__init__()

    def execute(self) -> int:
        return 1

class SlowFrameInferenceStrategy(InferenceStrategy):

    def __init__(self) -> None:
        super().__init__()

    def execute(self) -> int:
        return 5

class NoFrameInferenceStrategy(InferenceStrategy):

    def __init__(self) -> None:
        super().__init__()

    def execute(self) -> int:
        return 0
