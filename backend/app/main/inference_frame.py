

from abc import abstractclassmethod
from typing import Type

from app.main.instance_result import FrameInstanceResult


class InferenceFrame(object):
    def __init__(self):
        pass

    @abstractclassmethod
    def predict(self, frame) -> Type[FrameInstanceResult]:
        pass
