import os

basedir = os.path.abspath(os.path.dirname(__file__))


class Config:
    INFERENCE_SERVER_HOST = os.environ.get("INFERENCE_SERVER_HOST")
    INFERENCE_SERVER_PORT = os.environ.get("INFERENCE_SERVER_PORT")
    INFERENCE_SERVER_MODEL_NAME = os.environ.get("INFERENCE_SERVER_MODEL_NAME")

    SPLASH_IMAGE = os.environ.get("SPLASH_IMAGE", "no-feed.jpg")

    PAGE_TITLE = os.environ.get("PAGE_TITLE", "Google Distributed Edge")
    DISPLAYED_MODEL_NAME = os.environ.get(
        "DISPLAYED_MODEL_NAME", "Personal Protective Equipment (PPE) Detection"
    )
    DEFAULT_STREAM_URL = os.environ.get(
        "DEFAULT_STREAM_URL", "http://localhost:5000/static/hat-no-hat-demo.mp4"
    )

    INFERENCE_SERVER_MODEL_LABEL = os.environ.get(
        "INFERENCE_SERVER_MODEL_LABEL", None
    )  # matches the Label in tf-serving configuration
    INFERENCE_STRATEGY = os.environ.get(
        "INFERENCE_STRATEGY", "FULL_FRAME"
    )  # NO_FRAME, SLOW_FRAME, FULL_FRAME (default)

    INFERENCE_STRATEGY_NOFRAME = "NO_FRAME"
    INFERENCE_STRATEGY_SLOWFRAME = "SLOW_FRAME"
    INFERENCE_STRATEGY_FULLFRAME = "FULL_FRAME"

    LIGHT_SERVICE_IP = os.environ.get("LIGHT_SERVICE_IP")  # "192.168.1.200"
    LIGHT_SERVICE_STATE = os.environ.get("LIGHT_SERVICE_STATE")  # true / false

    LOG_LEVEL = os.environ.get("LOGLEVEL", "WARN").upper()

    INFERENCE_RESULT_THRESHOLD = int(
        os.environ.get("INFERENCE_RESULT_THRESHOLD", 40)
    )  # percentage of confidence to consider a match
    INFERENCE_RESIZE_PCT = int(
        os.environ.get("INFERENCE_RESIZE_PCT", 20)
    )  # percentage of original image size to send to inference server

    SHOW_INTERNET_STATUS = os.environ.get("SHOW_INTERNET_STATUS", "true")
