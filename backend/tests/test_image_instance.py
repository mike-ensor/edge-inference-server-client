import base64
import json
import cv2

from app.main.predict_request import ImageInstance, PredictRequest, b64Image

# Bash Version of  Base64
# VERY_SMALL_BEER_IMAGE = '/9j/4AAQSkZJRgABAQAAAQABAAD/4gHYSUNDX1BST0ZJTEUAAQEAAAHIAAAAAAQwAABtbnRyUkdCIFhZWiAH4AABAAEAAAAAAABhY3NwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAA9tYAAQAAAADTLQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAlkZXNjAAAA8AAAACRyWFlaAAABFAAAABRnWFlaAAABKAAAABRiWFlaAAABPAAAABR3dHB0AAABUAAAABRyVFJDAAABZAAAAChnVFJDAAABZAAAAChiVFJDAAABZAAAAChjcHJ0AAABjAAAADxtbHVjAAAAAAAAAAEAAAAMZW5VUwAAAAgAAAAcAHMAUgBHAEJYWVogAAAAAAAAb6IAADj1AAADkFhZWiAAAAAAAABimQAAt4UAABjaWFlaIAAAAAAAACSgAAAPhAAAts9YWVogAAAAAAAA9tYAAQAAAADTLXBhcmEAAAAAAAQAAAACZmYAAPKnAAANWQAAE9AAAApbAAAAAAAAAABtbHVjAAAAAAAAAAEAAAAMZW5VUwAAACAAAAAcAEcAbwBvAGcAbABlACAASQBuAGMALgAgADIAMAAxADb/2wBDABALDA4MChAODQ4SERATGCgaGBYWGDEjJR0oOjM9PDkzODdASFxOQERXRTc4UG1RV19iZ2hnPk1xeXBkeFxlZ2P/2wBDARESEhgVGC8aGi9jQjhCY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2P/wAARCAAYABgDASIAAhEBAxEB/8QAFgABAQEAAAAAAAAAAAAAAAAAAAUG/8QAIBAAAgIBAwUAAAAAAAAAAAAAAAECETEDBBIFEyFRgf/EABYBAQEBAAAAAAAAAAAAAAAAAAMCBf/EABwRAAIDAQADAAAAAAAAAAAAAAABAgMREgQxQf/aAAwDAQACEQMRAD8A37w6ySNbf7mHiMY4zbssEhQT3ahVpTr4mZ/m2WQcOH7eD0qL3pFcAGgACR3lDqEZcZU9RquLvNegALq4zxv49Lg80rAAcg//2Q=='
# Python BAse64 (not sure why different, but both result in image displayed)
VERY_SMALL_BEER_IMAGE = '/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAIBAQEBAQIBAQECAgICAgQDAgICAgUEBAMEBgUGBgYFBgYGBwkIBgcJBwYGCAsICQoKCgoKBggLDAsKDAkKCgr/2wBDAQICAgICAgUDAwUKBwYHCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgr/wAARCAAYABgDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD97L1mis5Z4YwZgtfJ3xB/au+O3h8Npmj6BpbKLXm4a5uftGfyr60OPnctuBXpXylb+HbLUfi7B4bTTxNDB4gMAz2tra5r+e/GviDijIcbk/8AZFe31iuqL+dj7Lg+hldf6x9eo3sr/wBfgfXMbbo1b1ANFJGMRqPRRRX9BQvyK+58aymqESLIenlV8oN8RbPQ/wBonT9Z/sy9MN54ourYRDRbrzs/afsu7H2bp36/jRRX5xx1keXZnVy6WIjd08VGcf8AFrqe9kk5JYj0Prheg+lFFFfpJ4J//9k='

def test_image_base64():
    img_bytes = b'x48\x69' # Hi
    expected = base64.b64encode(img_bytes).decode()

    base64_image = b64Image(img_bytes)

    assert base64_image.b64 == expected

def test_image_instance_init():
    img_bytes = b'x48\x69' # Hi

    instance = ImageInstance("hat", b64Image(img_bytes))

    assert instance.key == "hat"
    assert instance.image_bytes.b64 == base64.b64encode(img_bytes).decode()

def test_image_instance_real_image():
    # Read real image
    image = cv2.imread("app/static/very-small-beer.jpg", cv2.IMREAD_COLOR)
    bytes_image = cv2.imencode('.jpg', image)[1].tobytes()

    # Create real base64 image obj
    encodedImage = b64Image(bytes_image)
    assert encodedImage.b64 == VERY_SMALL_BEER_IMAGE
    # Create Image Instance
    instance = ImageInstance("hat", encodedImage)

    jsonOutput = instance.json()
    obj = json.loads(jsonOutput)

    assert obj.get("key") == "hat"
    assert obj.get("image_bytes").get("b64") == VERY_SMALL_BEER_IMAGE

def test_predict_requests_json():

    img_bytes = b'x48\x69' # Hi
    encodedImage = b64Image(img_bytes)
    instance = ImageInstance("hat", encodedImage)
    request = PredictRequest([instance])

    jsonOutput = request.json()
    obj = json.loads(jsonOutput)

    assert obj.get("instances")[0].get("key") == "hat"
    assert obj.get("instances")[0].get("image_bytes").get("b64") == base64.b64encode(img_bytes).decode()

def test_predict_requests_json_real_base64():

    image = cv2.imread("app/static/very-small-beer.jpg", cv2.IMREAD_COLOR)
    bytes_image = cv2.imencode('.jpg', image)[1].tobytes()
    # Create real base64 image obj
    encodedImage = b64Image(bytes_image)
    # Create Image Instance
    instance = ImageInstance("hat", encodedImage)

    # Create Request
    request = PredictRequest([instance])

    jsonOutput = request.json()
    obj = json.loads(jsonOutput)

    assert obj.get("instances")[0].get("key") == "hat"
    assert obj.get("instances")[0].get("image_bytes").get("b64") == VERY_SMALL_BEER_IMAGE

