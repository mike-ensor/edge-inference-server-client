from app.main.video import decode_stream_url


RSTP_FEED_B64="cnRzcDovL2NhbWVyYS11c2VyOmNhbWVyYS1leHBlciEtcGEkJEAxOTIuMTY4LjEuNjQ6NTU0L3N0cmVhbTE="
RSTP_FEED_STR="rtsp://camera-user:camera-exper!-pa$$@192.168.1.64:554/stream1"

def test_decode_stream_url():
    """ Test that the stream URL is decoded correctly"""
    assert decode_stream_url(RSTP_FEED_B64) == RSTP_FEED_STR