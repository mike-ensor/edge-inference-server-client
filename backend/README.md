# Overview

Goal is to make a backend server that uses Python to inference in the following methods:

1. Using a gRPC tf-serving instance with a vision model (can be tf-lite or TensorFlow, with or without GPU to be configured all within tf-serving)
1. Using an Edge TPU model (local)

## Running in K8s

This project is dependent on a `tf-serving` inference server to be configured and running in a kubernetes cluster (often the same cluster).

### Setup Tensorflow Serving
1. clone this repo: https://gitlab.com/gcp-solutions-public/retail-edge/available-cluster-traits/tensorflow-serving-anthos
1. `kubectl apply -f config/tf-serving` into the destination kubernetes cluster. The default configuration uses a public built image exported from an AutoML model detecting a hat (fry-chef hat)

### Running the app
1. Build and push the container to your GCP gcr.io registry (or use the default public image in the manifests)
  * Adjust the variables in the `./manifests/*.yaml` file to your repo or use the public default

1. `kubectl apply -f ./manifests`
1. Get the IP from the `LoadBalancer` for the backend app
    ```bash
    kubectl get svc -n default edge-demo -o json | jq -r ".status.loadBalancer.ingress[0].ip"
    ```
1. `http://<the IP address>:8080/`


## Running Server (locally)

`flask run`

> NOTE: This projects uses `.envrc` to set ENV variables, please `source .envrc` or use `direnv`


## Docker Builds

1. Recommend using `gcloud builds submit .` to build the docker container, but if buildling locally, follow the squence below

```
export VERSION="v1"
export PROJECT_ID="<some-gcp-project>"

docker build -t edge-demo .

docker tag edge-demo gcr.io/${PROJECT_ID}/edge-demo:${VERSION}

docker push gcr.io/${PROJECT_ID}/edge-demo:${VERSION}

```